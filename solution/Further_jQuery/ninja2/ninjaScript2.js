
$(document).ready(function(){
	//hide all elements with class = "description"
	$(".description").hide();
	
	//event handler for click events on all <h2> elements
	//
	$("h2").click(function(){
		//reveal or hide the next sibling element
		//over a one-second duration using a jQuery UI easing
		$(this).next().animate({height: "toggle"}, 500, "easeOutBounce");
		//for added fun, let's just explode the heading...
		//$(this).effect("shake", {times:3}, 330).hide("explode", {}, 1200);
	});
	
	
	
	//use mouse-event handlers to change the colour of <h2> elements
	$("h2").mouseover(function(){
		// change the CSS of the element
		$(this).css("color", "blue");
	});
	$("h2").mouseout(function(){
		// change the CSS of the element
		$(this).css("color", "orange");
	});
	

	

	//event handler for link elements inside list items
	$("li a").mouseover(function(){
		//animate for mouseover - highlight this element
		//by positioning the blank div correctly...
		//the div has z-axis of -1 in the CSS in order to appear underneath the link as a background
		$("#linkBlock").animate({
			width: $(this).width() + 12,
			left: $(this).position().left,
			top: $(this).position().top			
			}, 1000);
	});

	$("li a").mouseout(function(){
		//animate for mouseout - shrink the block to zero width, then position over the 1st link ready for next time
		//note the chaining of two animate functions...
		$("#linkBlock").animate({
			width: 0			
			}, 300).animate({
				left: $("li:first a").position().left
			});
	});
	
	//event handlers to create a 2-image slideshow...
	$("#alternatingPics").mouseover(function(){
		$("#alternatingPics img:last").stop().fadeIn();
	});
	$("#alternatingPics").mouseout(function(){
		$("#alternatingPics img:last").fadeOut();
	});
	
	
	//lighten the background colour of alternating columns in table
	//(this technique workds only if the number of columns is even)
	$("tr td:even").css("background-color", "#117711");
	//change the background colour of a row while it's being pointed at
	//note that the hover function combines mouseover and mouseout: it
	//therefore requires two functions as arguments, one for pointer moving onto
	//the element and one for leaving the element
	$("tbody tr").hover(function(){
		$(this).css("background-color", "#E05500");
	}, function(){
		$(this).css("background-color", "#004422");
	});

 	
});