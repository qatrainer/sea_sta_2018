package qa.java.sol.calculator;

import java.awt.*;
import java.awt.event.*;

public class Calculator2 extends Frame
{
	private TextField display;
	private String[] keyLabels = {
									"7", "8", "9", "*",
									"4", "5", "6", "/",
									"1", "2", "3", "-",
									"0", ".", "=", "+"
								 };
    private Button[] key;
	private boolean firstDigit = true;
	private double scratchpad = 0.0;
	private char operationChar = '=';

	private CheckboxMenuItem twoDPCheckboxMenuItem = null;
	private MenuItem contentsMenuItem = null;
	private MenuItem aboutMenuItem = null;
	private boolean twoDecPlaces = false;


	public Calculator2(String title)
	{
		super(title);

		setLayout(new BorderLayout(0, 10));
		display = new TextField("0");
		display.setEditable(false);
		display.setFont(new Font("Helvetica", Font.PLAIN, 20));
		add("North", display);

		Panel keyPanel = new Panel();
		keyPanel.setLayout(new GridLayout(4, 4, 5, 5));

		key = new Button[keyLabels.length];
		for (int i = 0; i < keyLabels.length; i++)
		{
		    key[i] = new Button(keyLabels[i]);
			keyPanel.add(key[i]);
		}
		add("Center", keyPanel);


		setUpListeners();

		setSize(200, 200);
		show();

		// Add a menu bar with two pull-down menus
		MenuBar mb = new MenuBar();

		Menu formatMenu = new Menu("Format");
		twoDPCheckboxMenuItem = new CheckboxMenuItem("2 decimal places");
		formatMenu.add(twoDPCheckboxMenuItem);

		Menu helpMenu = new Menu("Help");
		contentsMenuItem = new MenuItem("Contents");
		helpMenu.add(contentsMenuItem);
		helpMenu.addSeparator();
		aboutMenuItem = new MenuItem("About Calculator");
		helpMenu.add(aboutMenuItem);

		mb.add(formatMenu);
		mb.add(helpMenu);
		setMenuBar(mb);



}


	//
	// Override getInsets() method to inset all components
	// by 10 pixels from left, right and bottom edges of
	// container, and 50 pixels from top edge (to allow
	// for combined height of title bar and menu bar).
	//

	public Insets getInsets()
	{
		return new Insets(50, 10, 10, 10);
	}


    //
    //  define a class which implements ActionListener for entry keys
    //
    private class EntryListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// Append character to those displayed.
		// If it's entered immediately after an
		// operator key, clear display first.
            Object source = evt.getSource();
            if(source instanceof Button)
            {
			    String s = ((Button)source).getLabel();
				if (firstDigit)
				{
					display.setText(s);
					firstDigit = false;
				}
				else
					display.setText(display.getText() + s);
			}
        }
    }

    //
    //  define a class which implements ActionListener for operation keys
    //
    private class OperationListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// Save operator and contents of display (as a double);
            Object source = evt.getSource();
            if(source instanceof Button)
            {
			    String s = ((Button)source).getLabel();
    			char ch = s.charAt(0);
				// "+", "-", "*" or "/" key
				operationChar = ch;
				scratchpad = new Double(display.getText()).doubleValue();
				firstDigit = true;
			}
        }
    }


    //
    // define a class which implements ItemListener
    // for the "two decimal places" check box menu item
    //

    private class TwoDPCheckboxListener implements ItemListener
    {
        public void itemStateChanged(ItemEvent evt)
        {
            switch (evt.getStateChange())
            {
                case ItemEvent.SELECTED:
                    twoDecPlaces = true;
                    break;
                case ItemEvent.DESELECTED:
                    twoDecPlaces = false;
                    break;
                default:
                    break;
            }
        }
    }


    //
    // define a class which implements ActionListener
    // for the about menu item
    //

    private class AboutListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
				// pop up About dialog
				AboutDialog ab = new AboutDialog(Calculator2.this);
				ab.show();
        }
    }
/*
		else if (evt.target instanceof CheckboxMenuItem)
		{
			// Don't use arg, because its contents appear to
			// be inconsistent between Java 1.0 and Java 1.1
			if (evt.target  == twoDPCheckboxMenuItem)
			{
				// toggle twoDecPlaces flag
				twoDecPlaces = !twoDecPlaces;
			}
		}
		else if (evt.target instanceof MenuItem)
		{
			// Get label of menu item
			String s = (String) evt.arg;
			if (s.equals("About Calculator"))
			{
				// pop up About dialog
				AboutDialog ab = new AboutDialog(this);
				ab.show();
			}
			result = true;
		}


*/

    //
    //  define a class which implments ActionListener for the calculation key
    //
    private class CalculationListener implements ActionListener
    {
        public CalculationListener()
        {
        }
        public void actionPerformed(ActionEvent evt)
        {
		// If this was not entered
		// immediately after any operator, do the necessary
		// calculation and display the result
			if (!firstDigit)
			{
				doCalculation(new Double(display.getText()).doubleValue());
				if (twoDecPlaces)
					// round number up/down to 2 decimal places
					scratchpad = Math.round(scratchpad * 100)/100.0;
				display.setText("" + scratchpad);
				firstDigit = true;
			}

        }
    };

	//
	// setUpListeners{} method creates and registers
	// listeners for each key and for the dialog
	//

	public void setUpListeners()
	{
	    ActionListener entryListener = new EntryListener();
	    ActionListener operationListener = new OperationListener();
	    ActionListener calculationListener = new CalculationListener();
	    ItemListener twoDPCheckboxListener = new TwoDPCheckboxListener();
	    ActionListener aboutListener = new AboutListener();


	    //  for each key, create and register the appropriate type of listener
        for (int keyIndex = 0; keyIndex<keyLabels.length; ++keyIndex)
        {
            char ch = keyLabels[keyIndex].charAt(0);
			// If it's a number or the decimal point, register an EntryListener
			if (ch >= '0' && ch <= '9' || ch == '.')
			{
			    key[keyIndex].addActionListener(entryListener);
			}

			// If it's any operator apart from "=",  register an OperationListener
			else if (ch != '=')
			{
				// "+", "-", "*" or "/" key
			    key[keyIndex].addActionListener(operationListener);
			}

			// If it's the "=" operator, register a CalculationListener
			else
			{
				// "=" key
			    key[keyIndex].addActionListener(calculationListener);
			}
		}

		//  register listeners for menu items

		twoDPCheckboxMenuItem.addItemListener(twoDPCheckboxListener);
		aboutMenuItem.addActionListener(aboutListener);

		//  create and register a Listener to shut down when the window closes

		 WindowListener wl = new WindowAdapter()
                		    {
                		        public void windowClosing(WindowEvent evt)
                		        {
                		            dispose();
                		            System.exit(0);
                		        }
                		    };
		addWindowListener(wl);
	}

	//
	// Do the calculation
	//

	private double doCalculation(double operand)
	{

		switch (operationChar)
		{
			case '+':
				scratchpad += operand;
				break;
			case '-':
				scratchpad -= operand;
				break;
			case '*':
				scratchpad *= operand;
				break;
			case '/':
				scratchpad /= operand;
				break;
			case '=':
				scratchpad = operand;
				break;
		}
		return scratchpad;
	}

	//
	// Entry point
	//

	public static void main(String[] args)
	{
		Calculator2 cal = new Calculator2("Calculator");
	}


}
