package qa.java.sol.files;

import java.io.*;

public class LibraryFileEditor {
	
	public static final String fileName = "C:\\qajavnc1\\Solutions\\Files_and_Exceptions\\files\\members.txt";

	// QA To Do Step 5: Introduce try{} and catch()
	// Wrap the 2 calls in main in a try {} block and catch the IOException 
	// with a block that does nothing e.g {}
	// Ensure the code compiles
	// Having seen that you are not actually forced to do anything
	// lets display  "Problem with file handling." to System.err
	// Now run the application supplying a name and a numeric age
	
	// QA To Do Step 1: Make 2 method calls from Main 
	// In 'main' Call the inputMember() method which will when coded 
	// ask for a new member.
	// Call the showMembers() method which will when coded 
	// display the contents of the file. Compile.
	
	public static void main(String[] args) {
	    try {
		String reply = getInput("Want to quit?");
		while (!reply.equalsIgnoreCase("Quit"))
		{
			try {
				inputMember();
				showMembers();
			}
            		catch (FileNotFoundException e) {
				System.err.println("File not Found");
			}
			catch (IOException e)
			{
				System.err.println("Problem with file handling.");
			}
		
	// QA To Do Step 6: Handling the non numeric data
	// Run the application again this time supplying a non numeric age
	// Result - The JVM crashed with a stack trace due to 
	// the NumberFormatException thrown by Integer.parseInt()
	// Note that     a) you did not 'try' Integer.parseInt()
	//  nor did you  b) 'pass the buck' 
	//                   by coding 'throws NumberFormatException'
	// NumberFormatException is an unchecked runtime exception
	// that the compiler will ignore but we ought to catch it here
	// Code a catch block now, here, at this point that prints 
	// to System.err       e.getMessage() + " is an invalid age"
	// Compile and test again with non numeric age and
	// see the message including duff age printed in red (System.err)
	// When this is completed scroll down to Step 7 showMembers()
		catch (NumberFormatException e) {
			System.err.println(e.getMessage() + " is an invalid age");
		}
	
	// QA To Do Step 8: Testing File not found
	// The showMembers() method potentially throws either an 
	// IOException or a FileNotFoundException 
	// Code at this point here, now a catch of FileNotFoundException
	// displaying a suitable 'Not Found' message to System.err
	// Try and compile, it won't as 'code not reachable'
	// Place the catch immediately before the more generic 
	// catch of IOException up above
	// Then test the app after making a temporary typo in the filename
	// at top of class
	// Scroll to the bottom of the class for Step 9
	
	// QA To Do Step 11: Catch UnderAgeException and produce display
	// Add a catch of UnderAgeException and display to System.err
	// a message displaying the invalid age that is encapsulated 
	// in the exception plus the contents of the string passed 
	// into the constructor when it was thrown from inputMember()
	// Test now supplying an under age member
	// After this works scroll to Step 12 (at bottom of showMembers())
			catch (UnderAgeException tye) {
				System.err.println(tye.getAge() + " is " + tye.getMessage());
			}
		
	          reply = getInput("Input new member (or quit): ");
		  }       // end while
		}           // end try
		catch (IOException e) {
				System.err.println("Problem with file handling.");
		}
				
	}		// end of main()

	// QA To Do Step 4: Pass the buck
	// This method does not compile now as you are calling a method 
	// that throws but does not catch an IOException
	// You must catch it or pass the buck!
	// Pass the buck to main()
      // Try and compile, inputMember() will compile but main() won't!
      // Scroll up to Step 5

      // QA To Do Step 2: Call getInput() twice 
      // Call the getInput() method, asking for a name,
	// storing the return value in the variable 'name'.
	// Then call the getInput() method, asking for an age,
	// storing the return value in the variable 'strAge' 
	// Convert the return value to an int using method 'parseInt'
	// of class 'Integer' and store in the variable called 'age'
	// don't worry about any non-numeric data at this stage 
	// Compile
	
public static void inputMember() throws IOException, UnderAgeException {

	String name, strAge;
	int age;
	name = getInput("Enter name");
	strAge = getInput("Enter age");
	age = Integer.parseInt(strAge);
	
	// QA To Do Step 10: Deal with age less than 16 problem
	// If the 'age' is less than 16 then create and throw an instance 
	// of UnderAgeException passing the invalid age into the constructor
	// Compile
	// It fails because the exception extends Exception but does not
	// extend RunTimeException meaning it is a checked exception that must
	// be handled somewhere
	// Change the signature of this method to also 'throws' UnderAgeException
	// Scroll to Step 11 in main to add the corresponding catch

	
	if (age < 16)
		throw new UnderAgeException(age);

	}	// end of inputMember ()
	
      // QA To Do Step 3: Recieve keyboard data back from console
	// Print a message to the console asking the 'question'
	// Declare and create an InputStreamReader using 'System.in' 
	// as its constructor argument
	// Declare and create a BufferedReader using your InputStreamReader 
	// reference as its constructor argument
	// Use the BufferedReader's readLine() to retrieve your keyboard input
	// that is the string the method should return
	// Try and compile, it will fail. 
	// The IOException needs to be handled or the buck passed to the calling method 
	// Pass the buck!, declare the method as throwing IOException
	// Try and compile, getInput() will compile but the 2 calls to it
	// from inputMember() won't 
	// Scroll up to Step 4 to revisit inputMember()

	public static String getInput(String question) throws IOException {
		
		System.out.println(question);
		String keyboardinput = null;
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader bfr = new BufferedReader(isr);
		
			
		return keyboardinput = bfr.readLine();
	
	}	// end of getInput()
	
	// QA To Do Step 7: Read data from supplied .txt file
	// Declare and create a FileInputStream using instance variable
	// 'fileName' as its constructor argument 
	// Declare and create a InputStreamReader using your FileInputStream
	// reference as its constructor argument 
	// Declare and create a BufferedReader using your InputStreamReader 
	// reference as its constructor argument, call it 'bfr'
	// Read a Line of the file into the 'data' variable
	// While 'data' is not null (readLine() eventually returns null)
	//		- print it out to console and
	//      - read the next line into 'data'
	// Call close() on the BufferedReader when loop exits
	// Compile the code, several errors appear referring 
	// to 2 different exception types 
	// Lets 'pass the buck' you only need to 'throws IOException'
	// as it is the superclass of FileNotFoundException
	// The code now compiles and runs and displays the members
	// Scroll up to Step 8 in main
	public static void showMembers() throws IOException {
		String data;
		BufferedReader bfr = null;
		try {
			FileInputStream fis = new FileInputStream(fileName);
			InputStreamReader fr = new InputStreamReader(fis);
			bfr = new BufferedReader(fr);
			data = bfr.readLine();
			while (data != null) {
				System.out.println(data);
				data = bfr.readLine();
			}
		}
		finally
		{
			System.out.println("We're in finally");
			if (bfr != null)
			{
				bfr.close();
			}
				
		}
	// QA To Do Step 12: Introduce a finally {}
	// We have not ensured that the BufferedReader is closed
	// Place bfr.close() in a finally clause
	// Wrap all the rest of the method in a try {} block
	// Compile
	// It fails because 'bfr' is declared in and is local to the try!
	// Ensure that the BufferedReader is declared (just declared) prior to the 
	// 'try', you will also be asked to ensure that the 
	// reference is initialised (to null) prior to the 'try' block.
	// Put a System.out.println("We're in finally"); as 1st statement 
	// in the finally and ensure it appears whether the file 
	// is found or not.
	// You will find it does, but your code will crash in the finally
	// You can sort that one out yourself!!
	// If you get this all working go to the very bottom of the file
	// to find some very optional extra tasks 
		
	}	// end of showMembers()

}		// end of class

      // QA To Do Step 9: Write yor own Exception class
	// Write your own UnderAgeException class
	// It should
	// a) be package visible - no choice at this location
	// b) extend 'Exception'
	// c) have a private int age variable
	// d) have a public int getAge() method
	// e) have a constructor that receives an int and does 2 things
	//    i) passes the String "Too young for Library" to
	//        its superclass constructor
	//    ii) stores the int argument in instance variable 'age'
	// Compile and scroll up to Step 10 in the middle of inputMember() 
	
class UnderAgeException extends Exception
{
	private int age;
	public int getAge() {
		return age;
	}
	public UnderAgeException(int a)
	{
		super("Too young for Library");
		age = a;
	}
}

// QA To Do Very Optional steps
// 1)   Make the UnderAgeException an inner class
//      Just cut it out and drop it above the close brace } of the main class
//	  Did you get it working? the compilation error is slightly misleading
//      Think about what does an instance of an inner class need?

// 2)	Put the whole of main in a while loop
//      so that they can potentially enter lots of members
//      keeping prompting them "Type 'quit' to exit" and keep 
//      looping until they type in 'quit'. But allow them to type 
//      quit in upper case, lower case, mixed case etc
//      String reply = getInput("Type 'quit' if you want to exit");	
//	  would be a good 1st line of main before the while(....)
