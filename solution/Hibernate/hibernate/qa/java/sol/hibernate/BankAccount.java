package qa.java.sol.hibernate;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="bank_accounts")
public class BankAccount implements java.io.Serializable {

	@Id
	@Column(name="account_no")
	private int accountNumber;
	
	@Column(name="account_name")
	private String accountName;
	@Column(name="balance")
	private double balance;

	
	//every bank account has a list of transactions
	// N.B. mappedBy is the instance variable in BankTransaction that corresponds to the Foreign Key
	@OneToMany(targetEntity=qa.java.sol.hibernate.BankTransaction.class , 
			mappedBy="accountNumber", cascade=CascadeType.ALL) 
	@OrderBy("trans_no")
	private Set<BankTransaction> transactions;
	
	
	public Set<BankTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<BankTransaction> transactions) {
		this.transactions = transactions;
	}

	public BankAccount(){ }

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	/* business methods follow (along with an inner class) */
	
	protected boolean sufficentFundsAvailable(double amount){
		return (amount <= balance);
	}
	
	public void withdraw(double withdrawalAmount) throws InsufficientFundsException {
		// check there's enough money
		if (sufficentFundsAvailable(withdrawalAmount)){
			//generate a bank transaction
			BankTransaction bt = new BankTransaction();
			//set all properties EXCEPT the primary key, which will be auto-generated
			bt.setBankAccount(this);
			bt.setAccountNumber(accountNumber);
			bt.setAmount(withdrawalAmount);
			bt.setDebitOrCredit("D");
			bt.setDate(new Date());
			//attach the bank transaction to this account
			this.transactions.add(bt);
			//change the balance
			this.setBalance(balance - withdrawalAmount);
		} else {
			// not enough money, so...
			throw new InsufficientFundsException(this.accountNumber, this.accountName);
		}
	}

	
	public void deposit(double amount) {
		//generate a bank transaction
		BankTransaction bt = new BankTransaction();
		bt.setBankAccount(this);
		bt.setAccountNumber(accountNumber);
		bt.setAmount(amount);
		bt.setDebitOrCredit("C");
		bt.setDate(new Date());
		//attach the bank transaction to this account
		this.transactions.add(bt);
		//change the balance
		this.setBalance(amount + balance);
	}
	
	// static inner class to define an Exception condition...
	static class InsufficientFundsException extends Exception {
		
		public InsufficientFundsException() { super();}
		
		public InsufficientFundsException(int accountNumber, String accountName) {
			super("Insufficient funds to support requested operation on account no. "+
			accountNumber + " name: " + accountName);
		}
		
	} // end class InsufficientFundsException
	
	
}// end class BankAccount
