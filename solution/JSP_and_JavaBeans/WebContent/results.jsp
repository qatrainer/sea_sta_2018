<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>

<TITLE>Results Page</TITLE>

<jsp:useBean id='pb' class="qa.web.sol.festival.PlayBean" scope="request" >
	<jsp:setProperty name='pb' property="*" />
</jsp:useBean> 

</HEAD>
<BODY>
<H1>Showing on Day <jsp:getProperty name='pb' property='day' /></H1>
<DIV>
<UL>
<LI>Play: <jsp:getProperty name="pb" property="play" />
<LI>Playwright: <jsp:getProperty name="pb" property="author" />
<LI>Time: <jsp:getProperty name="pb" property="time" />
</UL>
</DIV>
<HR><DIV ALIGN="center"><A href="index.html">Back</A></DIV>
<BR>This page was processed: <%= new java.util.Date() %>.
</BODY>
</HTML>
