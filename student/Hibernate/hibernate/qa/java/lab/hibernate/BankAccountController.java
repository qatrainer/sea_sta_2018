package qa.java.lab.hibernate;
/* Here we can explore aspects of ORM with Hibernate
 * 1. Retrieve data using HQL from a single table, bank_transactions, which maps to an 
 * 	  entity class called BankTransaction.
 * 2. Retrieve data from two joined tables by using an HQL query
 *    on the association between two classes (BankAccount and BankTransaction)
 * 3. Update a bank account's name.
 * 4. Implement persistence for withdraw/deposit operations on bank accounts (updates 
 *    balance and creates a BankTransaction as a single unit of work, and the changes are 
 *    persisted "automatically" by the session).
 * 5. Add inheritance to the bank account model and read details from database.
 * 6. Implement polymorphic rule-checking in the withdraw operation.
 * 7. Add an Address data type (value type) to Customer
 * 
 *  Remember that there are scripts in the "labs" directory to re-create and re-populate the
 *  database, so that you can refresh the data at any time by running the scripts inside Toad
 *  (or from a command line via the mysql command if you really want to!).
 *  
 *      */

import java.util.*;

import javax.swing.JOptionPane;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.*;
import javax.swing.*;

public class BankAccountController {

	//the persistence manager object:
	private Session session;

	//initialise Hibernate and obtain the Session object
	public void init() {
		Configuration config = null;
		config = new Configuration();
		config.configure();
		
		ServiceRegistry sr = 
				new ServiceRegistryBuilder().
				applySettings(config.getProperties()).
				buildServiceRegistry();

		//define the mapping classes
		config.addAnnotatedClass(BankAccount.class);
		config.addAnnotatedClass(BankTransaction.class);
		
		SessionFactory sf = config.buildSessionFactory(sr);

		//use the SessionFactor to get a Session (store in instance var)
		session = sf.openSession();

	}
	
	public void close(){
		session.close();	
	}
	
	public void listAccounts(){
		// TO DO: run an HQL query on the BankAccount class

		// Remember to begin a transaction at the start and commit it at the end
		//
		// use the query's list() method to return a List<BankAccount>
		// iterate through the list and print out the details of each account
	}
	
	
	public void listAccountsAndTrans(){
		/*
		 * The BankAccount class has a variable called "transactions"
		 * of type Set<BankTransaction>. HQL treats this as the name of
		 * the association between the two classes. We can query by association 
		 * name, and HQL will figure out how to perform the relational join.
		 */
		// TO DO: run an HQL query on the BankAccount class joined to the BankTransaction class

		// Remember to begin a transaction at the start and commit it at the end
		//
		// use the query's list() method to return a List<Object[]>
		//
		// each object-array in the list represents a tuple (in this case a pair)
		// with one element per table
		
	}
	
	public void changeAccountDetails(){
		/* Method to change the state of one account
		 * Use a query with a named parameter to retrieve one 
		 * account as a POJO. Update the POJO. Then persist the
		 * change by flushing the session. (That's all that's needed.)
		 */
			
		// prompt for an account number
		int accNo = this.getInputAccountNumber("Account no. to display details of: ");

		// prompt for new name
		String newName = this.getInputName();
		
		// TO DO:
		//
		// Retrieve an account with the request account no. using either the session's get
		// method or a query that returns a unique result. Print out the details of the a/c.
		// Then set the name on the account object to newName, and persist the change.
		// (Hint: you might want to flush the session.)
		//

		
	}
	
	public void openAccount(){
		// all new accounts start with zero balance
		String newAccountName = this.getInputName();
		
		// TO DO
		// create a new bank account and use this name
		// (remember the primary key will auto-generate - you don't need to 
		// set it here)
		// 
		// use the session's save method to persist the new bank account
				
	}

	
	public void withdrawCash(){
		// withdraw money from an account
		// first, prompt the user for a/c no. and amount
		int accNo = this.getInputAccountNumber("Account no. to withdraw from: ");
		double amt = this.getInputAmount();
		System.out.println("ATTEMPTING WITHDRAWAL OF " + amt + "FROM A/C NO. " + accNo);
		Transaction tran = null;
		try {
			// transaction starts...		
			tran = session.beginTransaction();
			//fetch the a/c from the database
			BankAccount ba = (BankAccount) session.get(BankAccount.class, new Integer(accNo));
			// perform withdrawal on POJO
			try {
				ba.withdraw(amt);
			} catch (BankAccount.InsufficientFundsException ife){
				System.out.println("NOT ENOUGH MONEY! " + ife.getMessage());
			}
			session.flush();
			// transaction ends...
			session.getTransaction().commit();
		} catch(Exception e){
			// rollback if possible
			try {
				tran.rollback();
			} catch(HibernateException he){
				he.printStackTrace();
			}
			e.printStackTrace();
		}
	}// end method withdrawCash
	

	public void depositCash(){
		// deposit money int an account
		// first, prompt the user for a/c no. and amount
		int accNo = this.getInputAccountNumber("Account no. to deposit into: ");
		double amt = this.getInputAmount();
		System.out.println("ATTEMPTING DEPOSIT OF " + amt + " INTO A/C NO. " + accNo);
		Transaction tran = null;
		try {
			// transaction starts...		
			tran = session.beginTransaction();
			//fetch the a/c from the database
			BankAccount ba = (BankAccount) session.get(BankAccount.class, new Integer(accNo));
			// perform withdrawal on POJO
			ba.deposit(amt);
			session.flush();
			// transaction ends...
			session.getTransaction().commit();
		} catch(Exception e){
			// rollback if possible
			try {
				tran.rollback();
			} catch(HibernateException he){
				he.printStackTrace();
			}
			e.printStackTrace();
		}
	}// end method depositCash
	

	public void transferCash(){
		// withdraw money from one account and deposit in another
		// (as a single unit of work)
		// first, prompt the user for a/c nos. and amount
		int fromAccNo = this.getInputAccountNumber("Account no. to transfer funds from: ");
		int toAccNo = this.getInputAccountNumber("Account no. to transfer funds to: ");
		double amt = this.getInputAmount();
		System.out.println("ATTEMPTING TRANSFER OF " + amt + "FROM A/C NO. " + fromAccNo);
		Transaction tran = null;

		// TO DO 
		// 
		// Fetch the "from" a/c, perform the withdraw, then use session.flush() and session.clear()
		// to ensure the change is done (the database needs to auto-generate the key on the new 
		// bank_transaction row).
		// Now fetch the "to" a/c and perform the deposit, and commit the transaction.
		
		// All of the above must be in a "try" block.
		// In the "catch" block, perform a rollback, using tran.rollback();
		// Note that the rollback may itself throw a HibernateException, so you'll have to use
		// try/catch within the outer "catch".
		//
		
		
	}// end method transferCash
	
	// utility method: get input account number by prompting user
	public int getInputAccountNumber(String promptMsg){
		int accNo = 0;
		try {
			accNo = Integer.parseInt(JOptionPane.showInputDialog(promptMsg));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return accNo;
	}
	
	// utility method: get input name by prompting user
	public String getInputName(){
		String name = null;
		name = JOptionPane.showInputDialog("Please enter the new name");
		return name;
	}

	// utility method: get input money amount by prompting user
	public double getInputAmount(){
		double amount = 0;
		try {
			amount = Double.parseDouble(JOptionPane.showInputDialog("Please enter the amount to withdraw"));
		} catch(Exception e) {
			e.printStackTrace();
		}	
		return amount;
	}

	
	public static void main(String[] args) {
		// build an instance of this class and run its test methods
		BankAccountController bac = new BankAccountController();
		// initialise
		bac.init();
		// list all bank accounts
		//bac.listAccounts();
		// list all accounts with their associated transactions
		bac.listAccountsAndTrans();
		// change the details of one account, and redisplay all
		//bac.changeAccountDetails();
		//bac.listAccounts();
		// open a new bank account, and redisplay all
		//bac.openAccount();
		//bac.listAccounts();
		// make a withdrawal from one account, and redisplay all a/c's with transaction histories
		//bac.withdrawCash();
		//bac.listAccountsAndTrans();
		// make a deposit into one account, and redisplay all a/c's with transaction histories
		//bac.depositCash();
		//bac.listAccountsAndTrans();	
		// transfer money from one account to another, and redisplay all a/c's with transaction histories
		bac.transferCash();
		bac.listAccountsAndTrans();
		// finally, close the session
		bac.close();
	}

}
