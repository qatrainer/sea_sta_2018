package qa.java.lab.hibernate2;
import javax.persistence.*;

@Entity
//TO DO 
//
//set the discriminator value to "DA"
//
//Note: this class does NOT REQUIRE an @Table annotation because we're using 
//the single table strategy... the table is therefore automatically the same as 
//for the superclass.
//
public class CurrentAccount extends BankAccount implements java.io.Serializable {

	@Column(name="overdraft_limit")
	private double overdraftLimit;

	public double getOverdraftLimit() {
		return overdraftLimit;
	}

	public void setOverdraftLimit(double overdraftLimit) {
		this.overdraftLimit = overdraftLimit;
	}
	
	
	protected boolean sufficentFundsAvailable(double amount){
		boolean enough = false;

		//TO DO: Override the method that checks whether there's enough money 
		//to support the requested withdrawal amount - takE the OVERDRAFT LIMIT into account 
		
		return enough;
	}
	
}
