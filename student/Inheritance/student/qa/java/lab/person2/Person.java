/*
 *
 * Person
 *
 */
package qa.java.lab.person2;

public class Person
{
	// Instance variables for name and age
	private String name;
	private int age;

	//
	// Constructors
	//

	public Person()
	{
		name = "A N Other";
		age = 0;
	}


	public Person(String n, int a)
	{
		name = n;
		age = a;
	}

	//
	// Instance method to return name of person
	//
	public String getName()
	{
		return name;
	}


	//
	// Instance method to return details of person in a string
	// In this class, only detail is person's age
	//
	public String getDetails()
	{
		return "" + age;
	}

}
