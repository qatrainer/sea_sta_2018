
$(document).ready(function(){
	//hide all elements with class = "description"
	$(".description").hide();
	
	
	//
	// TO DO:
	// =====
	//
	// Add a click-event handler for click events on all <h2> elements
	//
	// We want to hide/reveal the next sibling element ( a paragraph)
	// on click. Use the animate() function with three argument: 
	// 1. toggle the height property
	// 2. specify a duration of 500 milliseconds or more
	// 3. specify a jQuery UI easing of "easeOutBounce"
	//
	
	
	//
	// TO DO:
	// =====
	//
	// Adding an explosion effect to the click-event handler on <h2> elements
	//
	// Just for fun, comment out the call to animate(), and 
	// replace it with a call to effect(), which takes 3 arguments, chained 
	// to a call to hide(), which also takes 3 arguments:
	// 
	// Args for effect():
	// 1. name of the effect - try "shake"
	// 2. a block specifying a name-value pair: set the property "times" to 3 or more
	// 3. specify a duration of 300 milliseconds or more
	//  
	// Args for hide():
	// 1. name of the effect - try "explode"
	// 2. an empty block
	// 3. specify a duration of 1000 milliseconds or more
	//  
	// ... Want to experiment? The inbuilt jQuery UI effects are:
	// blind, bounce, clip, drop, explode, fade, fold, highlight, puff, 
	// pulsate, scale, shake, size, slide, transfer.
	
	
	

	

	//event handler for link elements inside list items
	$("li a").mouseover(function(){

	//
	// TO DO:
	// =====
	//
	// select the "linkblock" <div> and call animate() on it.
	// It should appear underneath the <a> element that the user is 
	// pointing at, because of the z-axis value that you have specified
	// in the linked CSS file.
	//
	// Pass two arguments to the animate() function.
	// 
	// The first argument to pass to the animate() function is a block of code 
	// (an object containing name-value pairs fo properties).
	//
	// The three properties to set are width, left and top.
	//
	// The width of the block should be the same as the width of the 
	// <a> element, plus 12 pixels. The "left" property of the block 
	// should be equal to that of the <a>. (Hint  position().left on the 
	// the <a>.)
	// 
	// Finally, the "top" property of the linkBlock should be equal to 
	// that of the <a>.
	//
	// The second arg to pass the animate() is the duration in milliseconds. 
	// Try a value of 1000. 
	//

	
	});

	
	
	// This is the mouseout event handler corresponding to the mouseover you have implement yourself.
	// The mouseout should work OK as it is...
	//
	
	$("li a").mouseout(function(){
		//animate for mouseout - shrink the block to zero width, then position over the 1st link ready for next time
		//note the chaining of two animate functions...
		$("#linkBlock").animate({
			width: 0			
			}, 300).animate({
				left: $("li:first a").position().left
			});
	});
	
	
	//
	
	
	//event handlers to create a 2-image slideshow...
	$("#alternatingPics").mouseover(function(){
	
	//
	// TO DO:
	// =====
	//
	// Cause the last image in the "alternatingPics" <div> to fade into view.
	// Actually, call stop() and then chain fadeIn().
	
	});



	$("#alternatingPics").mouseout(function(){
	
	//
	// TO DO:
	// =====
	//
	// Cause the last image in the "alternatingPics" <div> to fade out of view.
	// 
	});
	
	
	
	
	
	// Mouse-event handlers to change the colour of <h2> elements
	$("h2").mouseover(function(){
		// change the CSS of the element
		$(this).css("color", "blue");
	});
	$("h2").mouseout(function(){
		// change the CSS of the element
		$(this).css("color", "orange");
	});
	
	//lighten the background colour of alternating columns in table
	//(this technique workds only if the number of columns is even)
	$("tr td:even").css("background-color", "#117711");
	
	//change the background colour of a row while it's being pointed at
	$("tbody tr").hover(function(){
		$(this).css("background-color", "#E05500");
	}, function(){
		$(this).css("background-color", "#004422");
	});

 	
});