<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>All Events</TITLE>
<jsp:useBean id='pb' class="qa.web.lab.festival.PlayBean" scope='session' />
</HEAD>
<BODY>
<H1>All Scheduled Events</H1>
<DIV>

<%
	int i=0;
	do {
		i++;
		pb.setDay(i);
		if (pb.getResultsObtained()) {
%>

<HR>
<UL>

<%-- Uncomment when you complete the bean!
<LI>Day: <jsp:getProperty name="pb" property="day" />
<LI>Play: <jsp:getProperty name="pb" property="play" />
<LI>Playwright: <jsp:getProperty name="pb" property="author" />
<LI>Time: <jsp:getProperty name="pb" property="time" />
--%>

</UL>

<%
		}
	} while (pb.getResultsObtained());
%>

</DIV>
<HR><DIV ALIGN="center"><A href="index.html">Back</A></DIV>
<BR>This page was processed: <%= new java.util.Date() %>.
</BODY>
</HTML>
