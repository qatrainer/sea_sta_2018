<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%--
		QA To do: Exercice 1 step 7
		
		Add a JSP page directive to import the Java
		package java.util.
--%>

</head>

<BODY>

<DIV align="center">
<H1>Festival of Culture</H1>
</DIV>

<%--
		QA To do: Exercice 1 step 9

		In place of the question marks, add JSP epressions
		to print the fullname, username, age and email.
		You get the values from request parameters. 
--%>
	

<%--
		QA To do: Exercice 1 step 10

		For the interests, use a bulleted list (<UL>...</UL>),
		with each list item (<LI>...</LI>) being one of the
		interests. The interests are obtained as an array of
		Strings from a request parameter. Obviously, you'll 
		need JSP scriptlets with a loop to do your bulleted
		list. If there aren't any interests (i.e. the array 
		has length 0), then print out 'none' in italics.
--%>

<DIV align="left">
<H2>Your details: </H2>

<TABLE border="0">
<TR>
<TD width="100" align="right"><B>Name:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right"><B>Age:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right"><B>Email:</B></TD>
<TD align="left">???</TD>
</TR>
<TR>
<TD width="100" align="right" valign="top"><B>Interests:</B></TD>
<TD align="left">
???
</TD>
</TR>
</TABLE>
</DIV>
<BR><BR><HR><BR>

<%--
		QA To do: Exercise 1 step 7
		
		Use an initialisation parameter in a JSP
		scriptlet to give e-mail contact details.
		Also print out the date when the page was
		processed.
--%>

<DIV>
Contact details: 
<A href="mailto:???">???</A>.
<BR>This page was processed at ???.
</DIV>

</body>
</html>



