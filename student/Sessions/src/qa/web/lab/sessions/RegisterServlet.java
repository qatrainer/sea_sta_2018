package qa.web.lab.sessions;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import java.text.*;

/**
 * @version 	1.0
 * @author lewis
 */
public class RegisterServlet extends HttpServlet {

	private static final NumberFormat nf = new DecimalFormat("00000");

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			
		/*
		 * QA To do: Exercise 3 step 3
		 * 
		 * For each of the request parameters fullname, age and email 
		 * (if they exist), create a Cookie object, set a reasonable 
		 * ‘maxAge’, and add it to the response.
		 */

		/*
		 * QA To do: Exercise 3 step 4
		 * 
		 * For the interests, recall that we obtain an array of Strings 
		 * from the getParameterValues() method. Using a loop, concatenate 
		 * these into one String, separated by single spaces, and send 
		 * this back in a Cookie.
		 */

		/*
		 * QA To do: Exercise 3 step 5
		 * 
		 * Obtain the username from a request parameter. If this exists, 
		 * and is not an empty String, generate a password using the method 
		 * generatePassword(). Set the password as a request attribute and 
		 * forward to the ‘/registerresults’ page (using a RequestDispatcher). 
		 * If the user failed to fill in a ‘username’ field in the form, 
		 * redirect them back to the register page to try again.
		 */


		String username = req.getParameter("username");
		if (username == null || username.equals("")) {
			resp.sendRedirect("register");
		} else {
			String password = generatePassword(username);
			System.out.println("The password is: " + password);
			req.setAttribute("password", password);
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/registerresults");
			rd.forward(req, resp);
		}
	}

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGet(req, resp);
	}
	
	private String generatePassword(String username) {
		if (username == null) return null;
		int hash = Math.abs(username.hashCode() % 100000);
		String hashString = nf.format(hash);
		return hashString;
	}

}
