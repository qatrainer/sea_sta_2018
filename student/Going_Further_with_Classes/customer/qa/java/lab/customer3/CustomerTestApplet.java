package qa.java.lab.customer3;

import java.applet.*;
import java.awt.*;

/*
 * CustomerTestApplet class
 *
 * CustomerTest is a Java applet for testing
 * the methods of the Customer class. Results are
 * displayed in a tabular format using a custom
 * component called QALiveTable.
 *
 * The Customer objects should be created in
 * the init() method
 *
 * You can use the displayCustomers() method to
 * display details of the customers in the applet
 *
 * You can use the testX() methods to test the
 * methods of your Customer class.
 *
 */

public class CustomerTestApplet extends Applet implements Testable
{
	// Reference to QALiveTable object that will display results
	QALiveTable table ;

	// Declare two customer variables
	private Customer customer1, customer2;




	//
	// The init() method is called when the applet is initialised
	//
    public void init()
    {

		//
		// Create two new Customer objects
		//
        customer1 = new Customer("Leonardo da Vinci");
        customer2 = new Customer("Robert Hooke");
		//
		//
		//
		// ToDo:
		//
		// Use overloaded constructor to initialise customers' name and status




		// Initialise the table for displaying the employee's details
		//
		initialiseDisplay(3, 3, 5); // 3 rows, 3 columns, 5 buttons
		displayHeadings("Customer", "A/C No.", "Status");

		//
		// Display the customers' details in the applet
		//
		displayCustomers();

	}

	//
	// This method displays record of each customer
	//
    void displayCustomers()
	{
		// Update customer 1 in row 1
		//
		updateCustomer(1, customer1.getName(), customer1.getAccountNumber(), customer1.getStatus());

		// Update customer 2 in row 2
		//
		updateCustomer(2, customer2.getName(), customer2.getAccountNumber(), customer2.getStatus());

	}

	//
	// This private method updates customer details in specified row
	//
	private void updateCustomer(int row, String name, int accountNumber, char status)
	{
		table.setRow(row, name, "" + accountNumber, "" + status);
	}






	//
	// The following testX() methods implement the Testable interface
	// and will be called by the QALiveTable component in response
	// to button clicks.  You can use them to call methods in your
	// Customer class.
	//

	public void test1()
	{
		//
		// ToDo:
		//
		// Call a method of customer 1



		//
		// Display the customers' details in the applet
		//
		displayCustomers();
	}

	public void test2()
	{
		//
		// ToDo:
		//
		// Call a method of customer 2



		//
		// Display the customers' details in the applet
		//
		displayCustomers();
	}


	public void test3()
	{
		//
		// ToDo:
		//
		// Call another method (same method name as in test1(), but an overloaded version)
		// on customer1


		//
		// Display the customers' details in the applet
		//
		displayCustomers();
	}


	public void test4()
	{
		//
		// ToDo:
		//
		// Call another method (same method name as in test2(), but an overloaded version)
		// on customer2


		//
		// Display the customers' details in the applet
		//
		displayCustomers();
	}


	public void test5()
	{
		//
		// ToDo:
		//
		// Call a class method to reset last-used a/c no.



		//
		// ToDo:
		//

		// build 2 new customer objects, holding their references
		// in the old variables... the previous objects are simply discarded...
        // customer1 = ...
        // customer2 = ...
		//




		// Display the customers' details in the applet
		// The account numbers should now be in the new range, starting at one more than the
		// value passed to the class method above
		displayCustomers();
	}


	//
	// The following methods set up the QALiveTable
	// component. You do not need to modify either
	// of them.
	//

	//
	// This private method simply calls the appropriate
	// method in the QALiveTable control to set up the
	// column headings
	//
	private void displayHeadings(String s1, String s2, String s3)
	{
		table.setColumnHeadings(s1, s2, s3);
	}


	//
	// This private method creates a QALiveTable component
	// and adds it to the applet container
	//
	private void initialiseDisplay(int rows, int cols, int buttons)
	{
		//
		// Create the QALiveTable component for displaying the results.
		// Pass this as last argument to make this
		// class the "target" object of the control panel,
		// i.e. the object that implements the Testable
		// interface.
		//
		table = new QALiveTable(rows, cols, buttons, this);

		// Ensure QALiveTable component fills applet container
		setLayout(new GridLayout(1, 1));

		// Add the QALiveTable component to applet container
		add(table);
	}




}


