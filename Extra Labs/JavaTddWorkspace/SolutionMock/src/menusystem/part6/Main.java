package menusystem.part6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		Demo demo = new Demo(System.out, reader); 
		try {
			demo.run();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
