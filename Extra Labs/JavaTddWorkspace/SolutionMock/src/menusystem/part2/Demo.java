package menusystem.part2;

import java.io.PrintStream;

public class Demo {

	private PrintStream out;

	public Demo(PrintStream out) {
		this.out = out;
	}

	String mainMenu() {
		StringBuffer output = new StringBuffer();
		output.append("1: Main menu (this)\n");
		output.append("4: Exit\n");
		return output.toString();		
	}

	public void run() {
		out.println(mainMenu());
	}

}
