package menusystem.part3;

import static org.easymock.EasyMock.endsWith;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.startsWith;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import org.easymock.EasyMockSupport;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

// Initial simulation of user interaction; mock reading from System.in
public class DemoTest extends EasyMockSupport {

	private Demo demo;
	private PrintStream mockOut;
	private BufferedReader mockReader; 


	@Before
	public void setUp() throws Exception {
		mockOut = createNiceMock(PrintStream.class);
		mockReader = createNiceMock(BufferedReader.class);
		demo = new Demo(mockOut, mockReader);
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), Matchers.startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), Matchers.endsWith("4: Exit\n"));
	}

	@Test
	public void run_prints_mainMenu_toConsole() throws IOException {
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
		replayAll();
		demo.run();
		verifyAll();
	}

	@Test
	public void userExitsDemoImmediately() throws IOException {
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
		expect(mockReader.readLine()).andReturn("4");
		mockOut.println(startsWith("Thank you"));
		expectLastCall(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
}
