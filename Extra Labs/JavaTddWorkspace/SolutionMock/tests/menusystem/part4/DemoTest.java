package menusystem.part4;

import static org.easymock.EasyMock.endsWith;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.startsWith;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import org.easymock.EasyMockSupport;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

// Specify utility method in Demo which robustly converts keyboard input to int
// DRY in test: extract expectInitialMenu()
// Initial logic to handle user input: 1
public class DemoTest extends EasyMockSupport {

	private Demo demo;
	private PrintStream mockOut;
	private BufferedReader mockReader; 

	@Before
	public void setUp() throws Exception {
		mockOut = createNiceMock(PrintStream.class);
		mockReader = createNiceMock(BufferedReader.class);
		demo = new Demo(mockOut, mockReader);
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), Matchers.startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), Matchers.endsWith("4: Exit\n"));
	}

	@Test
	public void run_prints_mainMenu_toConsole() throws IOException {
		expectInitialMenu();
		replayAll();
		demo.run();
		verifyAll();
	}

	@Test
	public void userExitsDemoImmediately() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("4");
		mockOut.println(startsWith("Thank you"));
		expectLastCall(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
	
	@Test
	public void readAnIntReturnsValidInt() throws IOException {
		expect(mockReader.readLine()).andReturn("1");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(1));
	}
	
	@Test
	public void readAnIntGivenInvalidReturnsMinus1() throws IOException {
		expect(mockReader.readLine()).andReturn("");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(-1));
	}

	@Test
	public void userRequestsMainMenu() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1");
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	private void expectInitialMenu() {
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
	}
}
