package menusystem.part7;

import static org.easymock.EasyMock.endsWith;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.startsWith;
import static org.junit.Assert.assertThat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import org.easymock.EasyMockSupport;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

// Handle new menu item: 2 (user registration)
// i. direct test of utitlity method to convert user interaction at keyboard
// into User instance.  ii. test in context of demo.run()
@SuppressWarnings("unused")
public class DemoTest extends EasyMockSupport {

	private Demo demo;
	private PrintStream mockOut;
	private BufferedReader mockReader; 

	@Before
	public void setUp() throws Exception {
		mockOut = createMock(PrintStream.class);
		mockReader = createMock(BufferedReader.class);
		demo = new Demo(mockOut, mockReader);
	}

	@Test
	public void mainMenuContent() {
		assertThat(demo.mainMenu(), Matchers.startsWith("1: Main menu (this)\n"));
		assertThat(demo.mainMenu(), Matchers.endsWith("4: Exit\n"));
	}

	@Test
	public void userExitsDemoImmediately() throws IOException {
		expectInitialMenu();
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	@Test
	public void readAnIntReturnsValidInt() throws IOException {
		expect(mockReader.readLine()).andReturn("1");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(1));
	}
	
	@Test
	public void readAnIntGivenInvalidReturnsMinus1() throws IOException {
		expect(mockReader.readLine()).andReturn("");
		replayAll();
		
		int entered = demo.readAnInt();
		verifyAll();
		assertThat(entered, Matchers.is(-1));
	}

	@Test
	public void userRequestsMainMenu() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1");
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall(); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	@Test
	public void userRequestsMainMenuTwice() throws IOException {
		expectInitialMenu();
		expect(mockReader.readLine()).andReturn("1").times(2);
		mockOut.println(startsWith("1: Main menu (this)\n"));
		expectLastCall().times(2); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
	
	@Test // @Ignore("To illustrate coverage")
	public void badUserInputIsCaught() throws IOException {
		expectInitialMenu(); 
		expect(mockReader.readLine()).andReturn("7");
		mockOut.println(startsWith("Sorry, unrecognised input"));
		expectLastCall(); 
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}
	
	@Test
	public void registerUserReturnsAUser() throws IOException {
		expectUserRegistration();
		replayAll();
		
		User registered = demo.registerUser();
		verifyAll();
		assertThat(registered.getName(), Matchers.is("fred"));
		assertThat(registered.getPwd(), Matchers.is("secret"));
	}
	
	@Test 
	public void userRegisters() throws IOException {
		expectInitialMenu(); 
		expect(mockReader.readLine()).andReturn("2");
		expectUserRegistration();
		
		expectUserExits(); 
		replayAll();
		
		demo.run();
		verifyAll();
	}

	private void expectInitialMenu() {
		mockOut.println("Please enter a number:");
		expectLastCall(); 
		mockOut.println(endsWith("Exit\n"));
		expectLastCall();
	}
	
	private void expectUserExits() throws IOException {
		expect(mockReader.readLine()).andReturn("4");
		mockOut.println(startsWith("Thank you"));
		expectLastCall();
	}
	
	private void expectUserRegistration() throws IOException {
		mockOut.println("Please choose a user name:");
		expectLastCall(); 
		expect(mockReader.readLine()).andReturn("fred");
		mockOut.println("Please choose a password:");
		expectLastCall(); 
		expect(mockReader.readLine()).andReturn("secret");
	}
}
