package employees.part3;

import java.util.Calendar;

import static employees.part3.PayGrade.*;

public class Developer extends Employee {

	public Developer(String name, Calendar start, String ssn, Address home) {
		super(name, start, ssn, home);
	}
	
	@Override
	protected PayGrade calculateGrade() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -4);

		if (start.before(cal)) 
			return F;
		
		cal.add(Calendar.YEAR, +1);
		if (start.before(cal)) 
			return E;
		
		cal.add(Calendar.YEAR, +1);
		if (start.before(cal)) 
			return D;
		
		cal.add(Calendar.YEAR, +1);
		if (start.before(cal)) 
			return C;
		
		return B;
	}
}
