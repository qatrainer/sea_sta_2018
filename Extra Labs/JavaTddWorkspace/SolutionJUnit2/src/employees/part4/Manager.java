package employees.part4;

import java.util.Calendar;

import static employees.part4.PayGrade.*;

public class Manager extends Employee {

	public Manager(String name, Calendar start, String ssn, Address home) {
		super(name, start, ssn, home);
	}
	
	@Override
	protected PayGrade calculateGrade() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -3);
		
		if (start.before(cal)) 
			return H;
		
		cal.add(Calendar.YEAR, +1);
		if (start.before(cal)) 
			return G;
		
		cal.add(Calendar.YEAR, +1);
		if (start.before(cal)) 
			return F;
		
		return E;
	}

	@Override
	public int getBonus() {
		int multiplier = (grade == E) ? 1 :
			 			 (grade == F) ? 2 : 
			 			 (grade == G) ? 3 : 
			 				 		    4;
		return 200 * multiplier;
	}
}
