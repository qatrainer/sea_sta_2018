package employees.part1;

// Options: 1, 2
@SuppressWarnings("unused")
public class Address {

	public static final String USFORMAT = "[0-9]{5}";
	public static final String UKFORMAT = "[A-Z]{1,2}[0-9]{1,2}\\s?[0-9][A-Z]{2}";

	private final String line1;
	private final String city;
	private final String postalCode;
	private final Country code;
	
	
	// 1. Could create our own InvalidAddressException, 
	// make it a checked exception: the constructor declares that it throws
	public Address(String line1, String city, String postalCode, Country code) {
		
		validatePostalCode(postalCode, code);
		
		this.line1 = line1;
		this.city = city;
		this.postalCode = postalCode;
		this.code = code;
	}

	
	// Refactor -> extract method.  
	// 2. Could make this protected, open it up for testing for 
	// further corner cases etc. 
	private void validatePostalCode(String postalCode, Country code) {
		
		switch (code) {
			case US:
				if (!postalCode.matches(USFORMAT)) 
					throw new IllegalArgumentException("Bad zip code: " + postalCode);
				break;
			case UK:
				if (!postalCode.matches(UKFORMAT)) 
					throw new IllegalArgumentException("Bad post code: " + postalCode);
				break;
			default:
				break;
		}
	}
}
