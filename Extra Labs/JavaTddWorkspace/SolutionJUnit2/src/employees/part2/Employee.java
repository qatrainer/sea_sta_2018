package employees.part2;

import java.util.Calendar;

public class Employee {

	public static final String SSNFORMAT = "[A-Z]{2}\\s?\\d{2}\\s?\\d{2}\\s?\\d{2}\\s?[A-Z]";

	protected String name;
	protected Calendar start;
	protected String ssn;
	protected Address home;
	
	public Employee(String name, Calendar start, String ssn, Address home) {
		if (! ssn.matches(SSNFORMAT)) 
			throw new IllegalArgumentException("Bad SSN: " + ssn);
		this.name = name;
		this.start = start;
		this.ssn = ssn;
		this.home = home;
	}
	
	public String getName() {
		return name;
	}

	// Note how toString() for Employee delegates to toString() for Address
	@Override
	public String toString() {
		return name + " [" + home + "]";
	}

	// Improved equals() definition over auto-generated version
	// - abstracts from case differences in SSN format
	// Even better would be to strip out irrelevant whitespace differences
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		return this.ssn.equalsIgnoreCase(other.ssn);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	public Country getLocation() {
		return home.getCountry();
	}
}
