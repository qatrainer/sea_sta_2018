package employees.part3;

import static employees.part3.Country.UK;
import static employees.part3.Country.US;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class AddressTest {
	
	@Test
	public void usAddressValidZipCodeDoesNotThrow() {
		Address usAddress = new Address("1001 Main Street", "Springfield", "12345", US);
		assertThat(usAddress, notNullValue(Address.class));
	}

	@Test(expected=IllegalArgumentException.class)
	public void usAddressBadZipCodeThrows() {
		new Address("1001 Main Street", "Springfield", "123456", US);
	}
	
	@Test
	public void usAddressBadZipCodeThrowsWithMessage() {
		try {
			new Address("1001 Main Street", "Springfield", "123456", US);
		}
		catch(IllegalArgumentException expected) {
			assertThat(expected.getMessage(), is("Bad zip code: 123456"));
		}
	}
	
	@Test
	public void ukAddressValidPostCodeDoesNotThrow() {
		new Address("12 Acacia Avenue", "London", "E10 7RJ", UK);
	}

	@Test(expected=IllegalArgumentException.class)
	public void ukAddressBadPostCodeThrows() {
		new Address("12 Acacia Avenue", "London", "E10 7R", UK);
	}
	
	@Test
	public void addressStringForm() {
		Address acacia = new Address("12 Acacia Avenue", "London", "E10 7RJ", UK);
		assertThat(acacia.toString(), is("12 Acacia Avenue, London, E10 7RJ"));
	}
}
