package employees.part4;

import static employees.part4.Country.UK;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Test;

public class ApprenticeTest {
	
	@Test(expected=IllegalArgumentException.class)
	public void employeeBadSSNThrows() {
		Address acacia = new Address("12 Acacia Avenue", ", London", "E10 7RJ", UK);
		Calendar cal = Calendar.getInstance();
		new Apprentice("Jim Joggs", cal, "AB 12 34 56", acacia);
	}

	@Test
	public void employeeValidSSNDoesNotThrow() {
		Employee jim = createJim();
		assertThat(jim, notNullValue(Employee.class));
	}

	@Test
	public void apprenticeHiredForLess1YearIsAtLowestGrade() {
		Employee jim = createJim();
		assertThat(jim.getGrade(), is(PayGrade.A));
	}

	@Test
	public void apprenticeHiredForLess1YearHasBasicSalary() {
		Employee jim = createJim();
		assertThat(jim.getSalary(), is(18000));
	}

	@Test
	public void apprenticeHiredForOver6MonthsGetsBasicBonus() {
		Employee jim = createJim();
		assertThat(jim.getBonus(), is(100));
	}

	private Employee createJim() {
		Address acacia = new Address("12 Acacia Avenue", "London", "E10 7RJ", UK);
		Calendar hired = Calendar.getInstance();
		hired.add(Calendar.MONTH, -7);
		return new Apprentice("Jim Joggs", hired, "AB 12 34 56 P", acacia);
	}
}