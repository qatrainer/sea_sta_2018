package demo.junit;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonTest {

    Person tom, dick, harry; 

    @Before
    public void setUp() throws Exception {
        tom = new Person("Tom", "Smith", 29);  
        dick = new Person("Dick", "Jones", 29);  
        harry = new Person("Harry", "Hole", 32);  
    }

    @Test
    public void testCompareTo() {
        assertEquals("Tom same age as Dick ", 0, tom.compareTo(dick));
        assertTrue("Tom younger than Harry", tom.compareTo(harry) < 0);
        assertTrue("Harry older than Dick",  harry.compareTo(dick) > 0);
    }

    @Test
    public void testToString() {
        assertEquals("Smith, Tom", tom.toString());
    }
}
