package util.part3;

public class SecondsToWords {

	public static String convertOriginal(int seconds) {
		String rv = "";
		int mins = seconds / 60;
		seconds %= 60;
		if (mins > 0) {
			String minEnd = (mins == 1) ? "" : "s";
			rv = mins + " minute" + minEnd + ", ";
		}
		String secEnd = (seconds == 1) ? "" : "s";  // code duplication!
		return rv + seconds + " second" + secEnd;
	}

	// refactored
	public static String convert(int seconds) {
		String rv = "";
		int mins = seconds / 60;
		seconds %= 60;
		if (mins > 0) {
			rv = pluralize("minute", mins) + ", ";
		}
		return rv + pluralize("second", seconds);
	}

	static String pluralize(String word, int number) {
		String suffix = (number == 1) ? "" : "s";
		return number + " " + word + suffix;
	}
}
