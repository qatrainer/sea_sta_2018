package util.part6;

public class SecondsToWords {

	// method definitely becoming too long!
	public static String convert(int seconds) {
		String rv = "";
		int hours = seconds / 3600;
		if (hours > 0) {
			rv += pluralise(hours, "hour");
			seconds = seconds % 3600;
			if (seconds == 0) return rv; 
			rv += ", ";						
		}
		int mins = seconds / 60;
		seconds = seconds % 60;
		if (mins > 0) {
			rv += pluralise(mins, "minute");
			if (seconds == 0) return rv; 
			rv += ", ";			
		}
		return rv + pluralise(seconds, "second");
	}

	static String pluralise(int number, String word) {
		String suffix = (number == 1) ? "" : "s";
		return number + " " + word + suffix;
	}
}
