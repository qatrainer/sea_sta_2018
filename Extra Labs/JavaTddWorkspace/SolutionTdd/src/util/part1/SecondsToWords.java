package util.part1;

public class SecondsToWords {

	// minimal - don't go beyond what the test specifies
	public static String convertInitial(int seconds) {
		return "0 seconds";
	}

	// improved
	public static String convert(int seconds) {
		return seconds + " seconds";
	}

}
