package demo.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class GettingStarted {

	public static void main(String[] args) {

		// Create a new instance of the HTML unit driver.
		// Notice that the remainder of the code relies on the interface, not the implementation.
		WebDriver driver = new HtmlUnitDriver();

		// Use the driver object to visit Google.
		driver.get("http://www.google.com");

		// Find a text input element by its name.
		WebElement elem = driver.findElement(By.name("q"));

		// Enter something to search for.
		elem.sendKeys("Michu");

		// Submit the form. WebDriver will find the form for us from the element.
		elem.submit();

		// Check the title of the page.
		System.out.println("Title: " + driver.getTitle());
	}
}
