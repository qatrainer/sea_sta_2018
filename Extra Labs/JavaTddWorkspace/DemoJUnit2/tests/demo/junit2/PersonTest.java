package demo.junit2;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import static demo.junit2.IsAPostCode.isAPostCode;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PersonTest {

	Person tom, dick, harry; 

	@BeforeClass
	public static void oneOffSetup() {
		System.out.println("One-off setup operations...");
	}
	
	@AfterClass
	public static void oneOffTeardown() {
		System.out.println("One-off teardown operations...");
	}
	
	@Before
	public void setUp() throws Exception {
		tom = new Person("Tom", "Smith", 29);  
		dick = new Person("Dick", "Jones", 29);  
		harry = new Person("Harry", "Hole", 32);  
	}

	// Trivial examples here, just to illustrate Hamcrest matcher syntax.

	@Test
	public void demo_lessThan() {
		assertThat(tom.compareTo(harry), lessThan(0));
	}

	@Test
	public void demo_equalTo() {
		assertThat(tom.getAge(), is(equalTo(dick.getAge())));
	}

	@Test
	public void demo_not() {
		assertThat(tom.getAge(), is(not(0)));
	}

	@Test
	public void demo_instanceOf() {
		assertThat(tom.toString(), instanceOf(String.class));
	}

	@Test
	public void demo_notNullValue() {
		assertThat(tom.toString(), notNullValue());
	}

	@Test
	public void demo_sameInstance() {
		Person ref1 = tom;
		Person ref2 = tom;
		assertThat(ref1, sameInstance(ref2));
	}

	@Test
	public void demo_extended_matchers() {
		// We've lumped all the asserts into the same test method here, just to show the syntax.
		assertThat(1, greaterThanOrEqualTo(0));
		assertThat(42, allOf(greaterThan(40), lessThan(50)));
		assertThat(0.1, closeTo(0.2, 0.1));
		assertThat("michu", equalToIgnoringCase("MICHU"));

		List<Person> listOfPersons = new ArrayList<Person>();
		listOfPersons.add(tom);
		listOfPersons.add(dick);
		listOfPersons.add(harry);
		assertThat(listOfPersons, hasItem(tom));
		assertThat(listOfPersons, hasItems(tom, dick));
	}
	
	@Test
	public void demo_custom_matcher() {
		assertThat("SA3 5BR", isAPostCode());
	}
}
