package stubsandmocks.version1;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class LotteryTest {

	// We could now test that all the members of the random set are distinct
	//     @Test public void randomNumbersAllDistinct() { ... }
	// 
	// But: 
	//     a) We would essentially be testing the java.util.Set class...
	//        Don't do that: test *your* code, not the libraries you use!
	//
	//     b) In order not to duplicate the logic in the class under test, 
	//        the test itself would introduce quite a bit of logic, needing testing.


	@Test 
	public void formatIsHyphenSeparatedSequenceOfNumbers() {
		Set<Integer> numbers = new HashSet<Integer>();
		numbers.addAll(Arrays.asList(20, 30, 40));
		assertTrue(Lottery.formatNumbers(numbers).matches("^\\d+ - \\d+ - \\d+$"));
	}
}
