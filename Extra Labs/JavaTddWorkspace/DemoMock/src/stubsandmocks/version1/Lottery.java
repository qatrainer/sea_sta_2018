package stubsandmocks.version1;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/*
 * Refactoring for testability (1): 
 * 
 * We've separated strategies for the following tasks into separate methods, to make the class more testable.
 * - Generating a single random number
 * - Generating a set of random numbers
 * - Formating the numbers in the correct format
 *
 * Note that the new methods have been given package-level visibility, to make them testable.
 */
public class Lottery {
	
	private static final int LOTTERY_SIZE = 6;
	private static final int HIGHEST_NUMBER = 49; 
	private static Random rand = new Random(); 
 
	public static void main(String[] args) {
		Set<Integer> numbers = generateRandomSet();		
		String result = formatNumbers(numbers);
		System.out.println(result);
	}

	static int generate(int limit) {
		return rand.nextInt(limit)+1; 
	}
	
	static Set<Integer> generateRandomSet() {
		Set<Integer> numbers = new HashSet<Integer>(LOTTERY_SIZE);
		while (numbers.size() < LOTTERY_SIZE) {
			numbers.add(generate(HIGHEST_NUMBER));
		}
		return numbers;
	}
	
	static String formatNumbers(Set<Integer> numbers) {
		int count = 0; 
		StringBuilder sb = new StringBuilder();	
		for (Integer number : numbers) {
			sb.append(number);
			if (++count < numbers.size()) sb.append(" - ");	
		}
		return sb.toString();
	}
}
