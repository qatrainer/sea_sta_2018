package stubsandmocks.version0;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/* 
 * Problem statement:
 * Write a program to simulate a lottery:  
 * generate 6 distinct random numbers in the range 1 to 49. 
 * Display the result in a string of exactly this format: "41 - 9 - 17 - 36 - 2 - 25"
 * 1st version: untestable
 */
public class Lottery {
	
	private static final int LOTTERY_SIZE = 6; 
	private static final int HIGHEST_NUMBER = 49;
	private static Random rand = new Random(); 

	public static void main(String[] args) {
		Set<Integer> numbers = new HashSet<Integer>(LOTTERY_SIZE);
		while (numbers.size() < LOTTERY_SIZE) { 
			numbers.add(rand.nextInt(HIGHEST_NUMBER)+1);
		}
		
		int count = 0; 
		StringBuilder sb = new StringBuilder();	
		for (Integer number : numbers) {
			sb.append(number);
			if (++count < numbers.size()) sb.append(" - ");	
		}
		System.out.println(sb.toString());
	}
}
