package stubsandmocks.version2; 

public class NumberGeneratorStub implements NumberGenerator {
	private int number = 0;
	
	public int generate(int limit) {
		return number++; 
	}
}