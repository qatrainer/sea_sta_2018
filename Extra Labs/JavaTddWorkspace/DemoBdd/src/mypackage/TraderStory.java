package mypackage;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.reporters.StoryReporterBuilder.Format;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

public class TraderStory extends JUnitStory  {

	@Override
	public Configuration configuration() {
		return super.configuration().useStoryReporterBuilder(
				new StoryReporterBuilder()
						.withDefaultFormats()
						.withFormats(Format.XML, Format.HTML, Format.STATS, Format.CONSOLE, Format.TXT)
						.withOutputDirectory("../build/jbehaveReports"));
	}
	
	public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new TraderSteps());
    }
}