package webdriver.part1;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class TestWebDriver {
	
	@Test
	public void isisdemo_goodUrl_findsPage() {
		WebDriver driver = new HtmlUnitDriver();
		driver.get("http://mmyco.co.uk:8180/isis-onlinedemo/wicket");
		assertThat(driver.getTitle(), equalTo( "Quick Start App") );
	}

	@Test
	public void isisdemo_badUrl_givesTomcatError() {
		WebDriver driver = new HtmlUnitDriver();
		driver.get("http://mmyco.co.uk:8180/not-isis-demo");
		assertThat( driver.getTitle(), Matchers.allOf( Matchers.startsWith("Apache Tomcat"),
				Matchers.endsWith("Error report") ) );
	}

	@Test
	public void isisdemo_goodUrl_showsLoginInputs() {
		WebDriver driver = new HtmlUnitDriver();
		driver.get("http://mmyco.co.uk:8180/isis-onlinedemo/wicket");
		assertThat(driver.getTitle(), equalTo( "Quick Start App") );

		// Check we see Login Panel
		List<WebElement> elts = driver.findElements(By.className("loginPanel"));	// We don't get exep if NotFound, so need to check:
		assertThat("Looking for element with className of 'loginPanel'", elts.size(), equalTo(1));

		// Find username, password input fields
		//  - Don't catch exceptions, missing element exception will cause test to fail
		driver.findElement(By.name("username"));
		driver.findElement(By.name("password"));
	}

	@Test
	public void isisdemo_goodLogin_showsLoggedInUsername() {
		WebDriver driver = new HtmlUnitDriver();
		driver.get("http://mmyco.co.uk:8180/isis-onlinedemo/wicket");
		assertThat(driver.getTitle(), equalTo( "Quick Start App") );

		// Check we see Login Panel
		List<WebElement> elts = driver.findElements(By.className("loginPanel"));	// We don't get an exception if Not Found, so need to check:
		assertThat("Looking for element with className of 'loginPanel'", elts.size(), equalTo(1));

		// Find username, password input fields
		WebElement inp_username = driver.findElement(By.name("username"));	// No need to catch exceptions, missing element exception will cause test to fail! ...
		WebElement inp_password = driver.findElement(By.name("password"));

		// Enter username & password
		inp_username.sendKeys("sven");
		inp_password.sendKeys("pass");
		inp_username.submit();

		// Look for  <span>sven</span>  inside of  <div id="secondaryMenu"> 
		// If not found, could mean we didn't log in properly.
		WebElement secMenu = driver.findElement(By.id("secondaryMenu"));	// No need to catch exceptions, missing element exception will cause test to fail!
		WebElement span = secMenu.findElements(By.tagName("span")).get(0);
		assertThat(span.getText(), equalTo("sven"));
	}
}
