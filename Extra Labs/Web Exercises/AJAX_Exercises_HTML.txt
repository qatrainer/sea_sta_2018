<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>Catalog search page</title>
<!-- <link rel="stylesheet" href="shopsite.css" type="text/css" >  -->

<!-- NOW BEGINS THE STYLE -->

<style>
body 
{
	background-color: #ffffff;
}

a:link
{
	color: #00a000;
}
a:visited
{  
	color: #006000;
}

div#TopLogo
{
	text-align:center;
}

div#TopLogo img
{
	width:558px;
	height:61px;
}

h1#MainTitle
{
	text-align: center;
	font-size: xx-large;
	font-family: Freestyle Script, Cursive, Fantasy;
	color: Green;
}

h2.SubTitle
{
	text-align:left;
	font-size:large;
}

div.MenuItem
{
	margin: 0.5ex 0ex;
}

span.MenuImage img
{
	vertical-align:middle;
}

span.MenuText
{
	font-size: 115%;
	font-weight: bold;
}

div#ButtonNavigation
{
	text-align:center;
	clear:both;
}

div#ButtonNavigation img
{
	border-width:0px;
	width:77px;
	height:32px;
}

#ProductSearchForm {
	width: 200px;
	float: left
}

#ProductInfoResults {
	width: 300px;
	height: 200px;
	float: left
}

</style>

<!-- NOW BEGINS THE SCRIPT -->

<script>
// Create a global AJAX object

// Send an AJAX request
function sendRequest(action, callback)
{

}

// Get product code by partial product name
function getProductNames()
{

}

function updateProducts()
{

}
</script>
</head>

<!-- NOW BEGINS THE HTML -->

<body>


<h1 id="MainTitle">Catalog Search</h1>

<h2 class="SubTitle">Search for product by type</h2>

<div id="ProductSearchForm">
<form id="ProductSearch">
   Product types are:<br />
   <select id="ProductTypes" onChange="">
   	  <option value="shirts">shirts</option>
	  <option value="skirts">skirts</option>
   	  <option value="trousers">trousers</option>
   </select>
</form>
</div>

<div id="ProductInfoResults">
<textarea id="ProductInfo" height="7" width="50">
</textarea>
</div>

</body>
</html>
